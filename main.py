from flask import Flask, render_template, request, redirect, session, flash,jsonify, render_template, url_for
import csv

app = Flask(__name__)
"""
Clave secreta. Esta debe ser aleatoria, para generar una, leer los siguientes pasos.
Abrir una terminal en la imagende python (dentro del docker) y ejecutar:
python
import os; print(os.urandom(16));
Esto nos imprime por pantalla una clave de este estilo
b'\x11\xad\xec\t\x99\x8f\xfa\x86\xe8A\xd9\x1a\xf6\x12Z\xf4'
Copiar y pega en la siguiente linea asignandosela a la variable
"""
app.secret_key = b"\xe9\xc8\xb9\xf4_\xa5\xe74\xfd\x08\xdc\x11\xb9\x07Ex"

## Definición de rutas de la App
# Protegida. Solo pueden entrar los que han iniciado sesión


@app.route("/escritorio")
def escritorio():
    return render_template("escritorio.html")

# Formulario para iniciar sesión

@app.route("/login")
def login():
    return render_template("login.html")

# Manejar login


@app.route("/hacer_login", methods=["POST"])
def hacer_login():
    correo = request.form["correo"]
    palabra_secreta = request.form["palabra_secreta"]
    if correo == "fabian@gmail.com" and palabra_secreta == "123":
        session["usuario"] = correo
        return redirect("/escritorio")
    else:
        flash("Correo o contraseña incorrectos")
        return redirect("/login")


@app.route("/logout")
def logout():
    session.pop("usuario", None)
    return redirect("/login")

@app.route("/vistas")
def vistas():
	return render_template('datatable.html')

# Verificamos si el usuario ha iniciado sesión
@app.before_request
def antes_de_cada_peticion():
    ruta = request.path
    
    if not 'usuario' in session and ruta != "/login" and ruta != "/hacer_login" and ruta != "/logout" and not ruta.startswith("/static"):
        flash("Inicia sesión para continuar")
        return redirect("/login")

# arma el json para ser utilizado con el datatable     

@app.route("/datatable_1")
def datatable_1():
	data = {}
	csvs = [row for row in csv.reader(open('/app/Clase02P/data/trips.csv', 'r'))]
	data['data'] = csvs
	return jsonify(data)

# Inicio el servidor
if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
