### El siguiente repositorio tiene como intención que el alumno pueda trabajar con un login + sesiones + datatable a partir de un archivo plano en formato .csv ya que utiliza una funcion para levantar los datos del mismo y construir un json para ser interpretado con ajax.

### Se pide leer el link que es parte de mucha mas información

    - https://www.jose-aguilar.com/blog/ajax-vs-json-respuesta-multiple/

### Actividades sugeridas

    - Clonar el repositorio y generar una rama con las actividades sugeridas
    - Crear un archivo en donde se almacene el usuario su contraseña encriptada de alguna manera, para que no este en el codigo como se muestra. Se sugiere un diseño que contemple por lo menos la siguiente estructura:
        - Cuenta de correo | password hasheado | activo | perfil | fecha de alta | fecha de activacion
    
    - Modificar la estructura del template para que pueda utilizar un archivo propio y no uno del ejemplo.